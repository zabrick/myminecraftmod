/*************************************************************
 ****************CODAKID MINECRAFT MODDING********************
 *************************************************************/

package mymod;

import mymod.CodakidFiles.BossModelWolf;
import mymod.CodakidFiles.Codakid;
import mymod._01_ChapterOne_ForgeYourSword.CustomMonster;
import mymod._01_ChapterOne_ForgeYourSword.CustomSword;
import mymod._02_ChapterTwo_StrikeTheEarth.CustomIngot;
import mymod._02_ChapterTwo_StrikeTheEarth.CustomOre;
import mymod._02_ChapterTwo_StrikeTheEarth.CustomPickaxe;
import mymod._02_ChapterTwo_StrikeTheEarth.LaserAxe;
import mymod._03_ChapterThree_PowerArmor.CustomArmor;
import mymod._04_ChapterFour_MakeSomeMonsters.CKModelRabbit;
import mymod._04_ChapterFour_MakeSomeMonsters.CKModelWither;
import mymod._04_ChapterFour_MakeSomeMonsters.CustomMob;
import mymod._04_ChapterFour_MakeSomeMonsters.CustomMob1;
import mymod._05_ChapterFive_RareLoot.ChanceBlock;
import mymod._07_ChapterSeven_ExplosionsAndSpecialEffects.CustomGrenade;
import mymod._08_ChapterEight_CustomStructures.StructureBlock;
import net.minecraft.block.Block;
import net.minecraft.client.model.ModelEnderman;
import net.minecraft.client.model.ModelZombie;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.item.ItemCoal;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.BiomeManager.BiomeEntry;
import net.minecraftforge.common.BiomeManager.BiomeType;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Main.MODID, name = Main.MODNAME, version = Main.VERSION)
public class Main
{
	public static final String MODNAME = "Nightbane Sword";
    public static final String MODID = "foundations";
    public static final String VERSION = "1.0";
    //hello computer
    //DECLARE VARIABLES
    public static Item mySword;
    public static ToolMaterial myToolMaterial;
    public static Item nightbanePickaxe;
    public static Block myOre;
    public static Item myIngot;
    public static Item laserAxe;
    public static Item myHelmet;
    public static Item myChestplate;
    public static Item myLeggings;
    public static Item myBoots;
    public static ArmorMaterial myArmorMaterial;
    public static Block myChanceBlock;

    
    @EventHandler
    public void preinit(FMLPreInitializationEvent event)
    {

    	//INITIALIZE VARIABLES
    	myToolMaterial = EnumHelper.addToolMaterial("Night Bane", 4, 8000, 12F, 10F, 60);
    	myArmorMaterial = EnumHelper.addArmorMaterial("Night Bane Armor", MODID + ":myArmor", 57, new int []{4, 8, 6, 3,}, 30);
    	mySword = new CustomSword();
    	nightbanePickaxe = new CustomPickaxe();
    	myOre = new CustomOre();
    	myIngot = new CustomIngot();
    	laserAxe = new LaserAxe();
    	myHelmet = new CustomArmor("myHelmet", 1, 0);
    	myChestplate = new CustomArmor("myChestplate", 1, 1);
    	myLeggings = new CustomArmor("myLeggings", 2, 2);
    	myBoots = new CustomArmor("myBoots", 1, 3);
    	myChanceBlock = new ChanceBlock ();
    	
    	
    	
    	
    	
    
    	
        

    	//REGISTER ITEMS AND BLOCKS
    	GameRegistry.registerItem(mySword, "mySword");
        GameRegistry.registerItem(nightbanePickaxe,"myPickaxe");
        GameRegistry.registerBlock(myOre, "myOre");
        GameRegistry.registerItem(myIngot, "myIngot");
        GameRegistry.registerItem(laserAxe, "laserAxe");
        GameRegistry.registerItem(myHelmet, "myHelmet");
        GameRegistry.registerItem(myChestplate, "myChestplate");
        GameRegistry.registerItem(myLeggings, "myLeggings");
        GameRegistry.registerItem(myBoots, "myBoots");
        GameRegistry.registerBlock(myChanceBlock, "chanceBlock");
        


    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {

    	//REGISTER TEXTURES
    	Codakid.registerItemTexture(mySword);
    	Codakid.registerItemTexture(nightbanePickaxe);
    	Codakid.registerBlockTexture(myOre);
    	Codakid.registerItemTexture(myIngot);
    	Codakid.registerItemTexture(laserAxe);
    	Codakid.registerItemTexture(myHelmet);
    	Codakid.registerItemTexture(myChestplate);
    	Codakid.registerItemTexture(myLeggings);
    	Codakid.registerItemTexture(myBoots);
    	Codakid.registerBlockTexture(myChanceBlock);

    	


    	//ADD RECIPES
    	GameRegistry.addShapedRecipe(new ItemStack(mySword),
    			" N ",
    			" N ",
    			" B ",
    			'N', myIngot,
    			'B', Items.blaze_rod);
    	

    	GameRegistry.addShapedRecipe(new ItemStack(nightbanePickaxe),
    			"NNN",
    			" B ",
    			" B ",
    			'N', myIngot,
    			'B', Items.blaze_rod);
    	
    	GameRegistry.addShapedRecipe(new ItemStack(myHelmet),
    			"NNN",
    			"N N",
    			"   ",
    			'N', myIngot);
    			
    			
    	
    	GameRegistry.addShapedRecipe(new ItemStack(myChestplate),
    			"N N",
    			"NNN",
    			"NNN",
    			'N', myIngot);
    			
    	
    	GameRegistry.addShapedRecipe(new ItemStack(myLeggings),
    			"NNN",
    			"N N",
    			"N N",
    			'N', myIngot);
    			
    	
    	GameRegistry.addShapedRecipe(new ItemStack(myBoots),
    			"   ",
    			"N N",
    			"N N",
    			'N', myIngot);
    			
    		  
    			
    			
    			
    			
    	
    	GameRegistry.addSmelting(myOre, new ItemStack(myIngot, 1), 13);
    
    	
    	//REGISTER MOBS
    	Codakid.registerMonster(CustomMonster.class, 0);
    	

    	EntityRegistry.registerGlobalEntityID(CustomMob.class, "myEntity", EntityRegistry.findGlobalUniqueEntityId(), 0xCD2400, 0x000000);
    	Codakid.registerEntity(CustomMob.class, new ModelEnderman(0), "slenderman");
    	
    	EntityRegistry.registerGlobalEntityID(CustomMob1.class, "myEntity1", EntityRegistry.findGlobalUniqueEntityId(), 0x4D35FF, 0x681600);
    	Codakid.registerEntity(CustomMob1.class, new ModelZombie(), "reanimated");

    	//REGISTER BIOMES

    
	}

	@EventHandler
    public void postinit(FMLPostInitializationEvent event)
    {



    }

}
