package mymod._08_ChapterEight_CustomStructures;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class StructureBlock extends Block {
	
	//Constructor
	public StructureBlock() {
		
		super(Material.rock);
		
	}
	
	//Method that happens when the block is spawned
	@Override
	public void onBlockAdded(World world, BlockPos pos, IBlockState state)
	{
		
	}

}
