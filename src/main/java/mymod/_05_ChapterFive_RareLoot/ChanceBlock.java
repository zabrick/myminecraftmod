package mymod._05_ChapterFive_RareLoot;

import java.util.Random;

import mymod.CodakidFiles.Codakid;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class ChanceBlock extends Block {

	//Constructor
	public ChanceBlock () {
		
		super(Material.rock);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setUnlocalizedName("chanceBlock");
		
	}
	
	//Make the block drop nothing when broken
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {
		
		return null;
		
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		Random rand = new Random();
		int number = rand.nextInt(100) + 1;
	    
	   // 5% spawn 15 diamonds
	   if (number <= 1)
	   {
		   Codakid.spawnItem(worldIn, pos, Items.diamond, 15);
	   }
	   // 10% spawn 20 iron ingot
	   else if(number <= 5)
	   {
		   Codakid.spawnItem(worldIn, pos, Items.iron_ingot, 20);
	   }
	   // 5% spawn 10 creepers
	   else if(number <= 25)
	   {
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityCreeper(worldIn));
		   
	   }
	   // 1% spawn 1 ender dragon
	   else if(number <= 26)
	   {
		   Codakid.spawnEntity(worldIn, pos, new EntityDragon(worldIn));
	   }
	   // 4% spawn 30 emeralds
	   else if(number <= 30)
	   {
		   Codakid.spawnItem(worldIn, pos, Items.emerald, 30);
	   }
	   // 30% spawn 40 zombie
	   else if(number <= 60)
	   {
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn)); 
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityZombie(worldIn));
	   }
		
	  // 5% spawn 10 ghast
	   else if(number <= 65)
	   {
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
		   Codakid.spawnEntity(worldIn, pos,new EntityGhast(worldIn));
	   }
	  // 10% spawn 12 squid
	   else if(number <=75)
	   {
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntitySquid(worldIn));
		   
	   }
	  // 10% spawn 5 eggs
	   else if(number <= 75)
	   {
		   Codakid.spawnItem(worldIn, pos, Items.egg, 5);
	   }
	  // 10% spawn 20 chickens
	   else if(number <= 15)
	   {
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
		   Codakid.spawnEntity(worldIn, pos, new EntityChicken(worldIn));
	   }
	  
	   
	}
	   
	

}
