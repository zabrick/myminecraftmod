package mymod._04_ChapterFour_MakeSomeMonsters;

import mymod.Main;
import mymod.CodakidFiles.Codakid;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.world.World;
 
public class CustomMob extends EntityMob {

	public float mobSpeed;
	public float attackDamage;
	public float followRange;
	public float health;
	
	public CustomMob(World worldIn) {
		super(worldIn);
		
		this.tasks.addTask(0, new EntityAIWander(this, this.mobSpeed));
		this.tasks.addTask(1, new EntityAISwimming(this));
		this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPlayer.class, 30));
		this.tasks.addTask(3, new EntityAIAttackOnCollide(this, EntityPlayer.class, this.mobSpeed, false));
		
		this.targetTasks.addTask(0, new EntityAIHurtByTarget(this, false, new Class[0]));
		this.targetTasks.addTask(1, new EntityAINearestAttackableTarget(this, EntityPlayer.class, true));
	}
	
	@Override
	public void applyEntityAttributes() {
		
		super.applyEntityAttributes();
		
		this.mobSpeed = 10f;
		this.attackDamage = 10f; 
		this.followRange = 25f;
		this.health = 100f;
		
		Codakid.setMovementSpeed(this, this.mobSpeed);
		Codakid.setAttackDamage(this, this.attackDamage);
		Codakid.setFollowRange(this, this.followRange);
		Codakid.setMaxHealth(this, this.health);

	}
	
	@Override
	public void dropFewItems(boolean flag, int x) {
		this.dropItem(Items.ender_pearl, 16);
		this.dropItem(Items.chainmail_chestplate, 1);
		this.dropItem(Main.myIngot, 2);
	}
	
	
}
