package mymod._04_ChapterFour_MakeSomeMonsters;

import mymod.CodakidFiles.Codakid;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class CustomMob1 extends EntityMob {

	private float mobSpeed;
	private float attackDamage;
	private float followRange;
	private float health;

	public CustomMob1(World worldIn) {
		super(worldIn);
		
		this.tasks.addTask(0, new EntityAIWander(this, this.mobSpeed));
		this.tasks.addTask(1, new EntityAISwimming(this));
		this.tasks.addTask(2, new EntityAIWatchClosest(this, EntityPlayer.class, 30));
		this.tasks.addTask(3, new EntityAIAttackOnCollide(this, EntityPlayer.class, this.mobSpeed, false));
		
		this.targetTasks.addTask(0, new EntityAIHurtByTarget(this, false, new Class[0]));
		this.targetTasks.addTask(1, new EntityAINearestAttackableTarget(this, EntityPlayer.class, true));
	}
	
	@Override
	public void applyEntityAttributes() {
		
		super.applyEntityAttributes();
		
		this.mobSpeed = 0.1F;
		this.attackDamage = 30F;
		this.followRange = 50F;
		this.health = 100F;
		
		Codakid.setMovementSpeed(this, this.mobSpeed);
		Codakid.setAttackDamage(this, this.attackDamage);
		Codakid.setFollowRange(this, this.followRange);
		Codakid.setMaxHealth(this, this.health);
		
	}
	
		
	}


