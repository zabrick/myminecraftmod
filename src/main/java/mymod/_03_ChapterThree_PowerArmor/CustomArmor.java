package mymod._03_ChapterThree_PowerArmor;

import mymod.Main;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class CustomArmor extends ItemArmor {
	
	public CustomArmor(String name,  int renderIndex, int armorType) {
		super(Main.myArmorMaterial,renderIndex, armorType);
		this.setCreativeTab(CreativeTabs.tabCombat);
		this.setUnlocalizedName(name);
			
	}
	
	@Override
	public void onArmorTick(World worldIn,EntityPlayer playerIn, ItemStack itemIn) {
		playerIn.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 23, 1 ));
		playerIn.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 34, 4 ));
		
		

	}
	
}
